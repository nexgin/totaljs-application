/**
 * Module configuration
 */
export interface ModuleConfig {
  application: string;
  label: string;
  name: string;
  mode: 'all' | 'input' | 'api';
  concurrency: number;
  redis?: string;
}

/**
 * Module state
 */
export type ModuleState = 'unconfigured' |
  'configuring' |
  'configured' |
  'registering' |
  'registered' |
  'unregistering' |
  'unregistered' |
  'initializing' |
  'initialized' |
  'booting' |
  'booted' |
  'starting' |
  'started' |
  'running' |
  'error' |
  'stopping' |
  'stopped';

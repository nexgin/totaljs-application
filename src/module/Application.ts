import { Queue, QueueEvents } from 'bullmq';
import { EventEmitter } from 'events';
import {
  AppHeartbeatMessage, AppHeartbeatResponse,
  AppMessages,
  AppRegisterMessage, AppRegisterResponse,
  AppUnregisterMessage, AppUnregisterResponse,
  ModMessages
} from '../messages';
import { Module } from '../Module';
import { QueueStats } from '../shared';

/**
 * Application class
 */
export class Application extends EventEmitter {
  /**
   * Application Queue (BullMQ Queue)
   */
  queue?: Queue;

  /**
   * Application QueueEvents (BullMQ QueueEvents)
   */
  queueEvents?: QueueEvents;

  /**
   * Application queue stats
   */
  queueStats: QueueStats = {
    pending: 0,
    completed: 0,
    failed: 0,
  };

  /**
   * Boolean flag representing if the module is registered with the application
   */
  registered: boolean = false;

  /**
   * A NodeJS timer for the heartbeat
   * @private
   */
  private heartbeat: NodeJS.Timeout | undefined;

  /**
   * Module constructor
   * - Binds the completed, failed, and doHeartbeat methods to this class
   * @param module - Module
   * @returns The application instance
   */
  constructor(public module: Module) {
    super();
    this.doHeartbeat = this.doHeartbeat.bind(this);
  }

  /**
   * Connects to the application
   * - Sets up the BullMQ Queue and QueueEvents
   * - Sets up the completed and failed event handlers
   */
  async connect() {
    this.queue = new Queue(
      this.module.config.application,
      { connection: this.module.redisClient }
    );
    this.queueEvents = new QueueEvents(
      this.module.config.application,
      { connection: this.module.redisClient }
    );
  }

  /**
   * Disconnects from the application
   * - If the queueEvents exists, remove all listeners and close the queueEvents
   * - If the queue exists, close the queue
   */
  async disconnect() {
    if (this.queueEvents) {
      this.queueEvents.removeAllListeners();
      await this.queueEvents.close();
    }
    if (this.queue) {
      await this.queue.close();
    }
  }

  /**
   * Sends a message to the application to register the module
   * - Builds an AppRegisterMessage
   * - Calls the application with the message
   * - If the response is successful, sets the registered flag to true and starts the heartbeat
   * - If the response is not successful, logs the error
   * - Returns the response
   * @returns The response from the application
   */
  async registerModule() {
    const registerMsg: AppRegisterMessage = {
      id: GUID(),
      name: 'ApplicationModuleRegister',
      data: {
        state: this.module.state,
        label: this.module.config.label,
        name: this.module.config.name,
      },
    };
    const registerResponse = await this.call(registerMsg) as AppRegisterResponse;
    this.registered = registerResponse.success;
    if (!this.registered) {
      console.log('Register Failed!', registerResponse);
    } else {
      this.heartbeat = setInterval(this.doHeartbeat, 5000);
    }
    return registerResponse;
  }

  /**
   * Sends a message to the application to unregister the module
   * - Builds an AppUnregisterMessage
   * - Calls the application with the message
   * - If the response is successful, sets the registered flag to false and stops the heartbeat
   * - If the response is not successful, logs the error
   * - Returns the response
   * @returns The response from the application
   */
  async unregisterModule() {
    const unregisterMsg: AppUnregisterMessage = {
      id: GUID(),
      name: 'ApplicationModuleUnregister',
      data: {
        state: this.module.state,
        label: this.module.config.label,
        name: this.module.config.name,
      },
    };
    const unregisterResponse = await this.call(unregisterMsg) as AppUnregisterResponse;
    this.registered = !unregisterResponse.success;
    if (this.registered) {
      console.log('Unregister Failed!', unregisterResponse.error);
    } else {
      if (this.heartbeat) {
        clearInterval(this.heartbeat);
      }
    }
    return unregisterResponse;
  }

  /**
   * Sends a message to the application to keep the module alive
   * - Builds an AppHeartbeatMessage
   * - Calls the application with the message
   * - If the response is not successful, logs the error
   * - Returns the response
   * @returns The response from the application
   */
  async doHeartbeat() {
    const unregisterMsg: AppHeartbeatMessage = {
      id: GUID(),
      name: 'ApplicationModuleHeartbeat',
      data: {
        state: this.module.state,
        label: this.module.config.label,
        name: this.module.config.name,
      },
    };
    const heartbeatResponse = await this.call(unregisterMsg) as AppHeartbeatResponse;
    if (!heartbeatResponse.success) {
      console.log('Heartbeat Failed!', heartbeatResponse);
    }
    return heartbeatResponse;
  }

  /**
   * Sends a message to the application and returns the results
   * - Increments the pending count
   * - Adds the message to the queue
   * - Waits for the job to finish and returns the response
   * @param modMsg - The message to send
   * @param ttl - The time to wait for a response
   * @returns The response from the application
   */
  async call(modMsg: AppMessages | ModMessages, ttl: number = 10000) {
    this.queueStats.pending++;
    const newJob = await this.queue?.add(modMsg.name, modMsg.data);
    if (newJob && this.queueEvents) {
      try {
        const response = await newJob.waitUntilFinished(this.queueEvents, ttl);
        this.queueStats.pending--;
        this.queueStats.completed++;
        return response;
      } catch (err) {
        this.queueStats.pending--;
        this.queueStats.failed++;
        console.log('Error waiting for job to finish', err);
        return { success: false, error: err };
      }
    }
  }
}

import { Job, Worker } from 'bullmq';
import { FlowMessage } from 'total4';
import {
  ModAPIMessage, ModAPIResponse,
  ModInvolvementMessage,
  ModInvolvementResponse, ModMessages,
  ModPhaseMessage,
  ModPhaseResponse
} from '../messages';
import { Module } from '../Module';

/**
 * ModuleWorker Class
 * This class is responsible for handling tasks sent to the module via BullMQ queues.
 * @class
 */
export class ModuleWorker {
  /**
   * BullMQ Worker
   */
  worker?: Worker;
  /**
   * BullMQ Instance Worker
   */
  instanceWorker?: Worker;

  /**
   * ModuleWorker constructor
   * - Binds methods used by the class
   * @param module - Module
   * @returns ModuleWorker
   */
  constructor(public module: Module) {
    this.drained = this.drained.bind(this);
    this.completed = this.completed.bind(this);
    this.failed = this.failed.bind(this);
    this.workHandler = this.workHandler.bind(this);
  }

  /**
   * Handles jobs from the queue sent from the application
   * - Logs the job
   * - Increments the pending stat
   * - Converts the job to a ModMessages object
   * - If the job is a ModulePhase message
   *   - Builds a ModPhaseResponse object
   *   - Calls the module's doNewMsg method 'phase' output and ModulePhase.data
   *     - If the response has output, add it to the ModPhaseResponse
   *     - If the response has meta, add it to the ModPhaseResponse
   *     - If the response has warnings, add it to the ModPhaseResponse
   *     - If the response has errors, add it to the ModPhaseResponse
   *     - If the response has stop, add it to the ModPhaseResponse
   *   - Return the ModPhaseResponse
   * - If the job is a ModuleInvolvement message
   *   - Calls the module's doNewMsg method 'involvement' output and ModuleInvolvement.data
   *   - Builds a ModInvolvementResponse object
   *   - If the response has meta, add it to the ModInvolvementResponse
   *   - Return the ModInvolvementResponse
   * - If the job is a ModuleAPI message
   *   - Calls the module's doNewMsg method 'api' output and ModuleAPI.data
   *   - Builds a ModAPIResponse object
   *   - Return the ModAPIResponse
   * - All other jobs return 'Unknown Job'
   * @param job - Job
   */
  async workHandler(job: Job) {
    console.log(`Module(${this.module.config.name}) Got Job ${job.name}`);
    this.module.stats.module.pending++;
    if (job.id) {
      const modMsg: ModMessages = {
        id: job.id,
        name: job.name as any,
        data: job.data,
      };
      if (modMsg.name === 'ModulePhase') {
        const response: ModPhaseResponse = {
          id: modMsg.id,
          name: modMsg.name,
          success: true,
          data: {
            phase: modMsg.data.phase,
          },
        };
        const processPhase: {
          output?: any;
          meta?: any;
          warnings?: string[];
          errors?: string[];
          stop?: boolean;
        } = await this.module.doNewMsg('phase', modMsg.data);
        if (processPhase.output) {
          response.data.output = processPhase.output;
        }
        if (processPhase.meta) {
          response.data.meta = processPhase.meta;
        }
        if (processPhase.warnings) {
          response.data.warnings = processPhase.warnings;
        }
        if (processPhase.errors) {
          response.data.errors = processPhase.errors;
        }
        if (processPhase.stop) {
          response.data.stop = processPhase.stop;
        }
        return response;
      }
      if (modMsg.name === 'ModuleInvolvement') {
        const involvementResponse = await this.module.doNewMsg('involvement', modMsg.data);
        const response: ModInvolvementResponse = {
          id: modMsg.id,
          name: modMsg.name,
          success: true,
          data: {
            phases: involvementResponse.phases,
          },
        };
        if (involvementResponse.meta) {
          response.data.meta = involvementResponse.meta;
        }
        return response;
      }
      if (modMsg.name === 'ModuleAPI') {
        const response: ModAPIResponse = {
          id: modMsg.id,
          name: modMsg.name,
          success: true,
          data: await this.module.doNewMsg('api', modMsg.data),
        };
        return response;
      }
    }
    return 'Unknown Job';
  }

  /**
   * Initializes the ModuleWorker
   * - Stops the worker to ensure it is not running
   * - Creates a new worker
   * - Binds the drained, completed, and failed methods to the worker
   * - Creates a new instance worker
   * - Binds the drained, completed, and failed methods to the instance worker
   */
  async init() {
    await this.stop();
    this.worker = new Worker(
      this.module.config.name,
      this.workHandler,
      { connection: this.module.redisClient, concurrency: this.module.config.concurrency || 10 }
    );
    this.worker.on('drained', this.drained);
    this.worker.on('completed', this.completed);
    this.worker.on('failed', this.failed);
    this.instanceWorker = new Worker(
      `${this.module.config.name}_${this.module.config.label}`,
      this.workHandler,
      { connection: this.module.redisClient, concurrency: this.module.config.concurrency || 10 }
    );
    this.instanceWorker.on('drained', this.drained);
    this.instanceWorker.on('completed', this.completed);
    this.instanceWorker.on('failed', this.failed);
  }

  /**
   * Boots the ModuleWorker, reserved for possible future use
   */
  async boot() {
    // TODO document why this async method 'boot' is empty
  }

  /**
   * Starts the ModuleWorker, reserved for possible future use
   */
  async start() {
    // TODO document why this async method 'start' is empty
  }

  /**
   * Readies the ModuleWorker, reserved for possible future use
   */
  async ready() {
    // TODO document why this async method 'ready' is empty
  }

  /**
   * Stops the ModuleWorker
   * - If the worker exists
   *   - Unbinds the drained, completed, and failed methods from the worker
   *   - Closes the worker
   *   - Sets the worker to undefined
   * - If the instance worker exists
   *   - Unbinds the drained, completed, and failed methods from the instance worker
   *   - Closes the instance worker
   *   - Sets the instance worker to undefined
   */
  async stop() {
    if (this.worker) {
      this.worker.off('drained', this.drained);
      this.worker.off('completed', this.completed);
      this.worker.off('failed', this.failed);
      await this.worker.close();
      this.worker = undefined;
    }
    if (this.instanceWorker) {
      this.instanceWorker.off('drained', this.drained);
      this.instanceWorker.off('completed', this.completed);
      this.instanceWorker.off('failed', this.failed);
      await this.instanceWorker.close();
      this.instanceWorker = undefined;
    }
  }

  /**
   * Handle the drained event from the BullMQ queue
   * - Sends a message to the 'drained' output with the message 'Drained'
   */
  drained() {
    // Queue is drained, no more jobs left
    const msg: FlowMessage = (this.module.instance as any).newmessage();
    msg.send('drained', {
      msg: 'Drained',
    });
  }

  /**
   * Handle the completed event from the BullMQ queue
   * - Decrements the pending count
   * - Increments the completed count
   * - Sends a message to the 'completed' output with the message 'Completed' and the job
   * @param job - Job
   */
  completed(job: Job) {
    // job has completed
    this.module.stats.module.pending--;
    this.module.stats.module.completed++;
    const msg: FlowMessage = (this.module.instance as any).newmessage();
    msg.send('completed', {
      job,
      msg: 'Completed',
    });
  }

  /**
   * Handle the failed event from the BullMQ queue
   * - Decrements the pending count
   * - Increments the failed count
   * - Sends a message to the 'failed' output with the message 'Failed', error, and job
   * @param job - Job
   * @param err - Error
   */
  failed(job: Job | undefined, err: Error) {
    // job has failed
    this.module.stats.module.pending--;
    this.module.stats.module.failed++;
    const msg: FlowMessage = (this.module.instance as any).newmessage();
    msg.send('failed', {
      job,
      msg: 'Failed',
      error: err,
    });
  }
}

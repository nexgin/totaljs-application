import { Redis } from 'ioredis';

/**
 * The state of a connection
 */
export type ConnectionState = 'disconnected' | 'connecting' | 'connected';

/**
 * Stats associated with queues
 */
export interface QueueStats {
  pending: number;
  completed: number;
  failed: number;
}

/**
 * A function to handle connecting to redis
 * @param config
 */
export async function connectRedis(config: string): Promise<Redis> {
  return new Promise((resolve, reject) => {
    if (config) {
      const redisClient = new Redis(config, { maxRetriesPerRequest: null });
      const error = (err: Error) => {
        reject(err);
      };
      const ready = async () => {
        redisClient.removeListener('error', error);
        resolve(redisClient);
      };
      redisClient.on('ready', ready);
      redisClient.on('error', error);
    }
  });
}

import { EventEmitter } from 'events';
import { Redis } from 'ioredis';
import * as RDB from 'rethinkdb';
import type { FlowInstance, FlowMessage } from 'total4';
import { ApplicationWorker } from './application/ApplicationWorker';
import { ModuleManager } from './application/ModuleManager';
import { ApplicationConfig, ApplicationState, ApplicationStats } from './application/shared';
import { ConnectionState, connectRedis } from './shared';

declare global {
  const REPO: any;
}
/**
 * Application class
 * This class is the main class for the Application component for use in Total4 Flow v10+.
 * It is responsible for managing the BullMQ queue and worker, managing the modules, managing
 * the status of the application and facilitating communication between the application and
 * the modules.
 * @class
 * @extends EventEmitter
 * @param {FlowInstance} instance - FlowInstance
 * @param {ApplicationConfig} config - ApplicationConfig
 */
export class Application extends EventEmitter {
  /**
   * The redis client for the application
   */
  public redisClient?: Redis;
  /**
   * The connection state of the redis client
   */
  public redisConnected: ConnectionState = 'disconnected';
  /**
   * The rethinkdb client for the application
   */
  public rethinkdbConnection?: RDB.Connection;
  /**
   * The connection state of the rethinkdb client
   */
  public rethinkdbConnected: ConnectionState = 'disconnected';

  /**
   * The state of the application
   */
  public _state: ApplicationState = 'unconfigured';
  /**
   * The stats of the application and modules
   */
  public _stats: ApplicationStats = {
    pending: 0,
    completed: 0,
    failed: 0,
    modules: [],
  };

  /**
   * The module manager for the application
   */
  public moduleManager: ModuleManager;

  /**
   * The application worker for the application
   */
  appWorker: ApplicationWorker;

  /**
   * The status updater for the application
   * @private
   */
  private statusUpdater?: NodeJS.Timeout;

  /**
   * The application data for the application
   */
  appData: any;

  /**
   * The error that occurred during initialization
   */
  initError?: Error;

  /**
   * Constructor for the Application class
   * This class is the main class for the Application component for use in Total4 Flow v10+.
   * This class is used by components/Application.html
   * - Creates this.appWorker and this.moduleManager
   * - Sets up the bindings for the instance
   * @param instance The Flow instance
   * @param config   The configuration for the application
   */
  constructor(public instance: FlowInstance, public config: ApplicationConfig) {
    super();
    this.moduleManager = new ModuleManager(this);
    this.appWorker = new ApplicationWorker(this);
    this.setUpBindings(instance);
  }

  /**
   * Get the state of the application
   */
  get state() {
    return this._state;
  }
  /**
   * Set the state of the application and emit the state event
   * @param state - The state of the application
   */
  set state(state: ApplicationState) {
    this._state = state;
    this.emit('state', state);
  }

  /**
   * Get the stats of the application and modules
   */
  get stats() {
    this._stats.modules = this.moduleManager.stats();
    return this._stats;
  }
  /**
   * Set the stats of the application and modules and emit the stats event
   * @param stats
   */
  set stats(stats: any) {
    this._stats = stats;
    this.emit('stats', stats);
  }

  /**
   * Set up the bindings for the instance
   * - Bind all necessary component instance methods to this class
   * - Setup the on('state') event handler and bind handlers to this class
   * @param instance - The Flow instance
   */
  setUpBindings(instance: FlowInstance) {
    (instance as any).input = this.input.bind(this);
    instance.message = this.message.bind(this);
    instance.configure = this.configure.bind(this);
    instance.close = this.close.bind(this);
    (instance as any).variables = this.variables.bind(this);
    (instance as any).variables2 = this.variables2.bind(this);
    (instance as any).trigger = this.trigger.bind(this);
    this.on('state', this.stateChanged.bind(this));
  }

  /**
   * Check the sanity of the application and throw an error if not sane to start
   * - Check if the application is already running and stop it if it is
   * - Check if the application has a name
   * - Check if the application has a rethinkdb config and connect to it
   * - Check if the application has a redis config and connect to it
   */
  async sanityCheck() {
    if (this.state === 'running') {
      await this.stop();
    }
    if (!this.config.name) {
      throw new Error('No name');
    }
    if (this.config.rethinkdb) {
      this.rethinkdbConnected = 'connecting';
      try {
        const connectionUrl = new URL(this.config.rethinkdb);
        this.rethinkdbConnection = await RDB.connect({
          host: connectionUrl.hostname,
          port: connectionUrl.port !== '' ? parseInt(connectionUrl.port, undefined) : 28015,
        });
        this.rethinkdbConnected = 'connected';
      } catch (e) {
        console.log('RethinkDB Error:', e);
        this.rethinkdbConnection = undefined;
        this.rethinkdbConnected = 'disconnected';
        throw new Error('No rethinkdb connection');
      }
    }
    if (this.config.redis) {
      this.redisConnected = 'connecting';
      try {
        this.redisClient = await connectRedis(this.config.redis);
      } catch (e) {
        console.log('Redis Error:', e);
        this.redisClient = undefined;
        this.redisConnected = 'disconnected';
        throw new Error('No redis connection');
      }
      this.redisConnected = 'connected';
    } else {
      throw new Error('No redis config');
    }
  }

  /**
   * Handle input coming from the flow stream
   * @param fromFlowStreamId - The id of the flow stream that sent the message
   * @param fromId - The id of the component that sent the message
   * @param data - The data that was sent
   * Currently this method is not used
   */
  input(fromFlowStreamId: string, fromId: string, data: any) {
    console.log('input', fromFlowStreamId, fromId, data);
    this.instance.send('events', data);
  }

  /**
   * Handles incoming messages from the flow stream
   * - Send the message data to the "phases" output and wait for the response
   * - Send the message and returned "phases" to the ModuleManager to process
   * - Send the message data to the "output" output
   * @param message - The message that was sent
   */
  message(message: FlowMessage) {
    // console.log('message', message);
    this.doNewMsg('phases', message.data).then((phases: string[]) => {
      return this.moduleManager.processInput(phases, message);
    }).then((response) => {
      console.log('Response:', response);
      message.send('output', message.data);
    }).catch((e) => {
      console.log('Error:', e);
    });
    return message;
  }

  /**
   * Configures the application. Called whenever the application configuration is changed
   * when the application is first created or when the application is reconfigured/restarted.
   * - Calls sanityCheck()
   * - Calls init()
   * - Calls boot()
   * - Calls start()
   * - Calls ready()
   * - If any of the above steps fail, the application is stopped and the error is logged
   *   and sent to "error" output
   */
  configure() {
    console.log('configure');
    this.state = 'configuring';
    this.sanityCheck().then(() => {
      return this.init();
    }).then(() => {
      return this.boot();
    }).then(() => {
      return this.start();
    }).then(() => {
      return this.ready();
    }).then(() => {
      console.log('Application Fully Ready');
    }).catch(async (e: Error) => {
      this.initError = e;
      const msg = (this.instance as any).newmessage();
      msg.send('error', { error: e.message });
      console.log('Initialization Error:', e);
      try {
        await this.stop();
      } catch (e) {
        console.log('Stop Error:', e);
      }
      this.state = 'error';
    });
  }

  /**
   * Shutdown the application. Called when the application is stopped or when the flow is stopped.
   */
  close() {
    console.log('close');
    this.stop().then(() => {
      console.log('Stopped Application');
    }).catch((e) => {
      console.log('Stop Error:', e);
    });
  }

  /**
   * FlowStream variables are changed
   * @param variables - The new variables
   */
  variables(variables: any) {
    console.log('variables', variables);
  }

  /**
   * Global variables are changed
   * @param variables - The new variables
   */
  variables2(variables: any) {
    console.log('variables2', variables);
  }

  /**
   * Trigger called from the component UI
   * Executes the configure() method
   */
  trigger() {
    console.log(`Application(${this.config.name}) trigger called`);
    this.configure();
  }
  // call(data: any, reply: any) {
  //   const arr = [];
  //   for (const key of Object.keys(REPO.mongodb)) {
  //     arr.push({ id: REPO.mongodb[key].string, name: REPO.mongodb[key].name });
  //   }
  //   reply(arr);
  // }

  /**
   * An even handler to monitor the state of the application
   * currently just logs the state to the console
   * @param state
   */
  stateChanged(state: ApplicationState) {
    console.log('stateChanged', state);
  }

  /**
   * Trigger a status update for the instance which is to be used by the components/Application.html
   */
  statusUpdateHandler() {
    this.instance.status({
      state: this.state,
      redisConnected: this.redisConnected,
      rethinkdbConnected: this.rethinkdbConnected,
      stats: this.stats,
    });
  }

  /**
   * Initialize the application
   * - Sets the state to "initializing"
   * - Calls the appWorker.init() method if the application is not in "input" mode
   * - Sets the stats to 0
   * - Calls the moduleManager.init() method
   * - Calls the doNewMsg() method to send the "init" message to the "init" output
   *   and stores the response in appData
   * - Sets the state to "initialized"
   * - Returns the appData
   */
  async init() {
    this.state = 'initializing';
    console.log('Initializing Application');
    this.statusUpdateHandler();
    if (this.config.mode !== 'input') {
      await this.appWorker.init();
    }
    this.stats.pending = 0;
    this.stats.completed = 0;
    this.stats.failed = 0;
    this.stats.modules = [];
    await this.moduleManager.init();
    this.appData = await this.doNewMsg('init', this.config);
    this.state = 'initialized';
    return this.appData;
  }

  /**
   * Boot the application
   * - Sets the state to "booting"
   * - Calls the appWorker.boot() method if the application is not in "input" mode
   * - Calls the moduleManager.boot() method
   * - Calls the doNewMsg() method to send the "boot" message to the "boot" output
   *   and stores the response in appData
   * - Sets the state to "booted"
   * - Returns the appData
   */
  async boot() {
    console.log('Booting Application');
    this.state = 'booting';
    if (this.config.mode !== 'input') {
      await this.appWorker?.boot();
    }
    await this.moduleManager.boot();
    this.appData = await this.doNewMsg('boot', this.appData);
    this.state = 'booted';
    return this.appData;
  }

  /**
   * Start the application
   * - Sets the state to "starting"
   * - Calls the appWorker.start() method if the application is not in "input" mode
   * - Calls the moduleManager.start() method
   * - Calls the doNewMsg() method to send the "start" message to the "start" output
   *   and stores the response in appData
   * - Sets the state to "started"
   * - Returns the appData
   */
  async start() {
    console.log('Starting Application');
    this.state = 'starting';
    if (this.config.mode !== 'input') {
      await this.appWorker?.start();
    }
    await this.moduleManager.start();
    this.appData = await this.doNewMsg('start', this.appData);
    this.state = 'started';
    return this.appData;
  }

  /**
   * Application is ready to process messages
   * - Calls the appWorker.ready() method if the application is not in "input" mode
   * - Calls the moduleManager.ready() method
   * - Calls the doNewMsg() method to send the "ready" message to the "ready" output
   *   and stores the response in appData
   * - Sets the state to "running"
   * - Returns the appData
   */
  async ready() {
    console.log('Application Ready');
    if (this.config.mode !== 'input') {
      await this.appWorker?.ready();
    }
    await this.moduleManager.ready();
    this.appData = await this.doNewMsg('ready', this.appData);
    this.statusUpdater = setInterval(this.statusUpdateHandler.bind(this), 5000);
    this.state = 'running';
    return this.appData;
  }

  /**
   * Stop the application
   * - Sets the state to "stopping"
   * - Clears the statusUpdater interval
   * - Calls the doNewMsg() method to send the "stop" message to the "stop" output
   *   and stores the response in appData
   * - Calls the appWorker.stop() method if the application is not in "input" mode
   * - Calls the moduleManager.stop() method
   * - Sets the state to "stopped"
   * - Returns the appData
   */
  async stop() {
    console.log('Stopping Application');
    this.state = 'stopping';
    if (this.statusUpdater) {
      clearInterval(this.statusUpdater);
    }
    this.appData = await this.doNewMsg('stop', this.appData);
    if (this.config.mode !== 'input') {
      await this.appWorker.stop();
    }
    await this.moduleManager.stop();
    await this.rethinkdbConnection?.close();
    this.state = 'stopped';
    return this.appData;
  }

  /**
   * Send a message out an output and once the message is complete return the data
   * from the message.
   * TotalJS Flow when sending messages are tracked for their lifetime. When a messages
   * life has ended the message triggers an "end" event. This is used to resolve the
   * promise.
   * @param output - the name of the output to send the message to
   * @param data - the data to send
   */
  async doNewMsg(output: string, data: any): Promise<any[]> {
    return new Promise((resolve) => {
      const msg = (this.instance as any).newmessage();
      msg.on('end', (m: any) => {
        resolve(m.data);
      });
      msg.send(output, data);
    });
  }

}

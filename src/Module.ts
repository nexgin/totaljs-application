import { EventEmitter } from 'events';
import { Redis } from 'ioredis';
import type { FlowInstance } from 'total4';
import { AppInputMessage, AppInputResponse } from './messages';
import { Application } from './module/Application';
import { ModuleWorker } from './module/ModuleWorker';
import { ModuleConfig, ModuleState } from './module/shared';
import { ConnectionState, connectRedis } from './shared';

declare global {
  // tslint:disable-next-line:function-name
  function GUID(): string;
}

/**
 * Module Class
 * This class is the main class of the Module component for use with Total4 Flow v10+.
 * It is used by components/Module.html to manage the module.
 * @class
 * @extends EventEmitter
 * @param instance - FlowInstance
 * @param config - ModuleConfig
 */
export class Module extends EventEmitter {
  /**
   * The application instance
   */
  application?: Application;
  /**
   * The module worker
   */
  moduleWorker?: ModuleWorker;
  /**
   * The redis client
   */
  public redisClient?: Redis;
  /**
   * The redis connection state
   */
  public redisConnected: ConnectionState = 'disconnected';
  /**
   * The module state
   * @private
   */
  private _state: ModuleState = 'unconfigured';
  /**
   * A timer to update the module status
   * @private
   */
  private statusUpdater?: NodeJS.Timeout;
  /**
   * The error that occurred during initialization
   * @private
   */
  private initError?: Error;
  /**
   * The module stats (includes app calls and module worker stats)
   */
  stats: {
    app: {
      pending: number;
      completed: number;
      failed: number;
    };
    module: {
      pending: number;
      completed: number;
      failed: number;
    };
  } = {
    app: {
      pending: 0,
      completed: 0,
      failed: 0,
    },
    module: {
      pending: 0,
      completed: 0,
      failed: 0,
    },
  };
  /**
   * The module data
   */
  moduleData?: any;

  /**
   * Module Constructor
   * - Sets up the bindings for the module
   * - Creates the module worker
   * @param instance
   * @param config
   */
  constructor(public instance: FlowInstance, public config: ModuleConfig) {
    super();
    (instance as any).input = this.input.bind(this);
    instance.message = this.message.bind(this);
    instance.configure = this.configure.bind(this);
    instance.close = this.close.bind(this);
    (instance as any).flowstream = this.flowStream.bind(this);
    (instance as any).variables = this.variables.bind(this);
    (instance as any).variables2 = this.variables2.bind(this);
    (instance as any).trigger = this.trigger.bind(this);
    this.on('state', this.stateChanged.bind(this));
    this.moduleWorker = new ModuleWorker(this);
  }

  /**
   * Get the state of the application
   */
  get state() {
    return this._state;
  }
  /**
   * Set the state of the application and emit the state event
   * @param state
   */
  set state(state: ModuleState) {
    this._state = state;
    this.emit('state', state);
  }

  /**
   * An even handler to monitor the state of the application
   * @param state
   */
  stateChanged(state: ModuleState) {
    console.log('stateChanged', state);
    this.instance.send('state', state);
  }

  /**
   * Trigger a status update for the instance which is to be used by the components/Module.html
   * - Get the application stats or set defaults to 0
   * - Set the status of the instance with object containing the state and stats properties
   */
  statusUpdateHandler() {
    this.stats.app = this.application?.queueStats || {
      pending: 0,
      completed: 0,
      failed: 0,
    };
    this.instance.status({ state: this.state, stats: this.stats });
  }

  /**
   * Check the sanity of the application and throw an error if not sane
   * - Check if the application is configured
   * - Check if the label is defined in the config
   * - Check if the name is defined in the config
   * - Check if the mode is defined in the config
   * - Check if the redis server is defined in the config
   * - Attempt to connect to the redis server
   */
  async sanityCheck() {
    if (!this.config.application) {
      throw new Error('No application configured');
    }
    if (!this.config.label) {
      throw new Error('No label defined in config');
    }
    if (!this.config.name) {
      throw new Error('No name defined in config');
    }
    if (!this.config.mode) {
      throw new Error('No mode defined in config');
    }
    if (!this.config.redis) {
      throw new Error('No redis server defined in config');
    }
    if (this.redisConnected !== 'connected') {
      this.redisConnected = 'connecting';
      this.redisClient = await connectRedis(this.config.redis);
      this.redisConnected = 'connected';
    }
  }

  /**
   * Handle input coming from the flow stream
   * - Log the input, only for debugging, possibly use or remove in the future
   * @param fromFlowStreamId
   * @param fromId
   * @param data
   */
  input(fromFlowStreamId: string, fromId: string, data: any) {
    console.log('input', fromFlowStreamId, fromId, data);
  }

  /**
   * Handle FlowStream events
   * - Log the event, only for debugging, possibly use or remove in the future
   * @param id
   * @param type
   */
  flowStream(id: string, type: string) {
    console.log('flowStream', id, type);
    // if (!id || (this.config.app && this.config.app.split('_')[0] === id)) {
    //   console.log(`Module(${this.config.name}) Got FlowStream Update`);
    //   this.instance.configure();
    // }
  }

  /**
   * Handles incoming messages from the flow stream
   * - Build an AppInputMessage (appMsg) from the message
   * - Call the application with the appMsg
   *   - If the call is successful
   *     - If appResponse.meta is defined, set the message.repo to appResponse.meta
   *     - Send the appResponse.data to "output"
   *   - If the call fails
   *     - Log the error
   *     - Destroy the message
   * @param message
   */
  message(message: any) {
    const appMsg: AppInputMessage = {
      id: GUID(),
      name: 'ApplicationModuleInput',
      data: {
        data: message.data,
        meta: message.repo,
      },
    };
    this.application?.call(appMsg).then((appResponse: AppInputResponse) => {
      if (appResponse.meta) {
        message.repo = appResponse.meta;
      }
      message.send('output', appResponse.data);
    }).catch((err) => {
      console.error(err);
      message.destroy();
    });
    return message;
    // console.log('message', message);
    // if (this.appFlowStreamId && this.appInstanceId) {
    //   (this.instance as any).toinput(message.data, this.appFlowStreamId, this.appInstanceId);
    // }
    // message.destroy();
    // return message;
  }

  /**
   * Configures the module. Called whenever the module configuration is changed
   * when the module is first created or when the module is reconfigured/restarted.
   * - Set the state to "configuring"
   * - Call sanityCheck
   * - If sanityCheck is successful
   *   - Create a new Application instance
   *   - Connect the application
   *   - Set the state to "registering"
   *   - Register the module with the application
   *   - If the application is not registered throw an error
   *   - If the application is registered send message to "registered" output
   *     - Set the state to "registered"
   *   - Call init
   *   - Call boot
   *   - Call start
   *   - Call ready
   * - If sanityCheck fails or any of the above steps fail
   *   - Set the state to "error"
   *   - Store error as initError property
   *   - Send message to "error" output
   * @param state
   * @param stateTitle
   */
  configure(state?: string, stateTitle?: string) {
    console.log('configure', state, stateTitle);
    this.state = 'configuring';
    this.sanityCheck().then(() => {
      this.application = new Application(this);
      return this.application.connect();
    }).then(() => {
      this.state = 'registering';
      return this.application?.registerModule();
    }).then(() => {
      if (!this.application?.registered) {
        throw new Error('Application not registered');
      }
      const msg = (this.instance as any).newmessage();
      msg.send('registered', {});
      this.state = 'registered';
      return this.init();
    }).then(() => {
      return this.boot();
    }).then(() => {
      return this.start();
    }).then(() => {
      return this.ready();
    }).then(() => {
      console.log(`Configured Module(${this.config.name})`);
    }).catch((e) => {
      this.state = 'error';
      this.initError = e;
      console.log(`Module(${this.config.name}) Initialization Error:`, e);
      const msg = (this.instance as any).newmessage();
      msg.send('error', e);
    });
  }

  /**
   * Shutdown the module. Called when the module is stopped or when the flow is stopped.
   */
  close() {
    console.log(`Close Module(${this.config.name})`);
    this.stop().then(() => {
      console.log(`Stopped Module(${this.config.name})`);
    }).catch((e) => {
      console.log('Stop Error:', e);
    });
  }

  /**
   * FlowStream variables are changed
   * @param variables
   */
  variables(variables: any) {
    console.log('variables', variables);
  }

  /**
   * Global variables are changed
   * @param variables
   */
  variables2(variables: any) {
    console.log('variables2', variables);
  }

  /**
   * Trigger called from the component UI
   * - Call the module's configure method
   */
  trigger() {
    console.log(`Module(${this.config.name} - ${this.config.label}) trigger called`);
    this.configure();
  }

  /**
   * Send a message out an output and once the message is complete return the data
   * from the message.
   * TotalJS Flow when sending messages are tracked for their lifetime. When a messages
   * life has ended the message triggers an "end" event. This is used to resolve the
   * promise.
   * @param output - The output to send the message to
   * @param data - The data to send
   */
  async doNewMsg(output: string, data: any): Promise<any> {
    return new Promise((resolve) => {
      const msg = (this.instance as any).newmessage();
      msg.on('end', (m: any) => {
        resolve(m.data);
      });
      msg.send(output, data);
    });
  }

  /**
   * Initialize the module
   * - Set the state to "initializing"
   * - If the module is not an input module
   *   - Call the moduleWorker's init method
   * - Call doNewMsg to send a message to the 'init' output
   *   - Pass the module's config as the data
   *   - Store the data returned from the message in the moduleData property
   * - Set the state to "initialized"
   */
  async init() {
    this.state = 'initializing';
    console.log(`Module(${this.config.name}) init`);
    if (this.config.mode !== 'input') {
      this.moduleWorker?.init();
    }
    this.moduleData = await this.doNewMsg('init', {
      config: this.config,
    });
    this.state = 'initialized';
  }

  /**
   * Boot the module
   * - Set the state to "booting"
   * - If the module is not an input module
   *   - Call the moduleWorker's boot method
   * - Call doNewMsg to send a message to the 'boot' output
   *   - Pass the moduleData as the data
   *   - Store the data returned from the message in the moduleData property
   * - Set the state to "booted"
   */
  async boot() {
    this.state = 'booting';
    console.log(`Module(${this.config.name}) boot`);
    if (this.config.mode !== 'input') {
      this.moduleWorker?.boot();
    }
    this.moduleData = await this.doNewMsg('boot', this.moduleData);
    this.state = 'booted';
  }

  /**
   * Start the module
   * - Set the state to "starting"
   * - If the module is not an input module
   *   - Call the moduleWorker's start method
   * - Call doNewMsg to send a "start" message to the module
   * - Set the state to "started"
   */
  async start() {
    this.state = 'starting';
    console.log(`Module(${this.config.name}) start`);
    if (this.config.mode !== 'input') {
      this.moduleWorker?.start();
    }
    this.moduleData = await this.doNewMsg('start', this.moduleData);
    this.state = 'started';
  }

  /**
   * Ready the module
   * - Set the state to "running"
   * - If the module is not an input module
   *   - Call the moduleWorker's ready method
   * - Call doNewMsg to send a message to the 'ready' output
   *   - Pass the moduleData as the data
   *   - Store the data returned from the message in the moduleData property
   * - Set up a status updater to send a status update every second to the UI
   */
  async ready() {
    this.state = 'running';
    console.log(`Module(${this.config.name}) ready`);
    if (this.config.mode !== 'input') {
      this.moduleWorker?.ready();
    }
    this.moduleData = await this.doNewMsg('ready', this.moduleData);
    this.statusUpdater = setInterval(this.statusUpdateHandler.bind(this), 5000);
  }

  /**
   * Stop the module
   * - Set the state to "stopping"
   * - Call doNewMsg to send a message to the 'stop' output
   *   - Pass the moduleData as the data
   *   - Store the data returned from the message in the moduleData property
   * - Have the application unregister the module
   * - If the module is not an input module
   *   - Call the moduleWorker's stop method
   * - Have the application disconnect
   * - Set the application to undefined
   * - Set the state to "stopped"
   * - If the statusUpdater is set
   *   - Clear the statusUpdater
   * - Call the statusUpdateHandler
   */
  async stop() {
    this.state = 'stopping';
    console.log(`Module(${this.config.name}) stop`);
    this.moduleData = await this.doNewMsg('stop', this.moduleData);
    await this.application?.unregisterModule();
    if (this.config.mode !== 'input') {
      this.moduleWorker?.stop();
    }
    await this.application?.disconnect();
    this.application = undefined;
    this.state = 'stopped';
    if (this.statusUpdater) {
      clearInterval(this.statusUpdater);
    }
    this.statusUpdateHandler();
  }
}

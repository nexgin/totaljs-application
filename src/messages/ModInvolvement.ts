import { AppMessage, AppMessageResponse } from './AppMessage';

export interface ModInvolvementMessage extends AppMessage {
  name: 'ModuleInvolvement';
  data: {
    phases: string[];
    meta?: any;
  };
}
export interface ModInvolvementResponse extends AppMessageResponse {
  name: 'ModuleInvolvement';
  data: {
    phases: {[phase: string]: number};
    meta?: any;
  };
}

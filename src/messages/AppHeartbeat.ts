import { ModuleState } from '../module/shared';
import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppHeartbeatMessage extends AppMessage {
  name: 'ApplicationModuleHeartbeat';
  data: {
    state: ModuleState;
    label: string;
    name: string;
  };
}
export interface AppHeartbeatResponse extends AppMessageResponse {
  name: 'ApplicationModuleHeartbeat';
}

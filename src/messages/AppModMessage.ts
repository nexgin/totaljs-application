import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppModMessage extends AppMessage {
  name: 'ApplicationModuleMessage';
  data: {
    name: string;
    data: any;
  };
}
export interface AppModMessageResponse extends AppMessageResponse {
  name: 'ApplicationModuleMessage';
}

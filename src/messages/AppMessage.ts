export interface AppMessage {
  id: string;
  name: string;
  data: any;
}
export interface AppMessageResponse {
  id: string;
  name: string;
  data?: any;
  success: boolean;
  error?: string;
}

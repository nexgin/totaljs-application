import { AppAPIMessage, AppAPIResponse } from './AppAPI';
import { AppHeartbeatMessage, AppHeartbeatResponse } from './AppHeartbeat';
import { AppInputMessage, AppInputResponse } from './AppInput';
import { AppModMessage, AppModMessageResponse } from './AppModMessage';
import { AppModQuery, AppModQueryResponse } from './AppModQuery';
import { AppRegisterMessage, AppRegisterResponse } from './AppRegister';
import { AppUnregisterMessage, AppUnregisterResponse } from './AppUnregister';
import { ModAPIMessage, ModAPIResponse } from './ModAPI';
import { ModInvolvementMessage, ModInvolvementResponse } from './ModInvolvement';
import { ModPhaseMessage, ModPhaseResponse } from './ModPhase';
export * from './AppHeartbeat';
export * from './AppInput';
export * from './AppModMessage';
export * from './AppModQuery';
export * from './AppRegister';
export * from './AppUnregister';
export * from './AppAPI';
export * from './ModInvolvement';
export * from './AppModMessage';
export * from './ModPhase';
export * from './ModAPI';

export type AppMessages =
  AppRegisterMessage |
  AppUnregisterMessage |
  AppHeartbeatMessage |
  AppAPIMessage |
  AppInputMessage |
  AppModMessage |
  AppModQuery;

export type AppMessageResponses =
  AppRegisterResponse |
  AppUnregisterResponse |
  AppHeartbeatResponse |
  AppAPIResponse |
  AppInputResponse |
  AppModMessageResponse |
  AppModQueryResponse;

export type ModMessages =
  AppModMessage |
  ModInvolvementMessage |
  ModPhaseMessage |
  ModAPIMessage;

export type ModMessageResponses =
  AppModMessageResponse |
  ModInvolvementResponse |
  ModPhaseResponse |
  ModAPIResponse;

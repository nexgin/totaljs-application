import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppModQuery extends AppMessage {
  name: 'ApplicationModuleQuery';
  data: {
    [attribute: string]: any;
  };
}
export interface AppModQueryResponse extends AppMessageResponse {
  name: 'ApplicationModuleQuery';
}

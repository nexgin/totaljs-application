import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppInputMessage extends AppMessage {
  name: 'ApplicationModuleInput';
  data: {
    data: any;
    meta?: any;
  };
}
export interface AppInputResponse extends AppMessageResponse {
  name: 'ApplicationModuleInput';
  meta?: any;
}

import { AppMessage, AppMessageResponse } from './AppMessage';

export interface ModPhaseMessage extends AppMessage {
  name: 'ModulePhase';
  data: {
    phase: string;
    input?: any;
    meta?: any;
  };
}
export interface ModPhaseResponse extends AppMessageResponse {
  name: 'ModulePhase';
  data: {
    phase: string;
    output?: any;
    meta?: any;
    warnings?: string[];
    errors?: string[];
    stop?: boolean;
  };
}

import { ModuleState } from '../module/shared';
import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppUnregisterMessage extends AppMessage {
  name: 'ApplicationModuleUnregister';
  data: {
    state: ModuleState;
    label: string;
    name: string;
  };
}
export interface AppUnregisterResponse extends AppMessageResponse {
  name: 'ApplicationModuleUnregister';
}

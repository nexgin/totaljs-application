import { AppMessage, AppMessageResponse } from './AppMessage';

/**
 * A message to make an API call to the application.
 */
export interface AppAPIMessage extends AppMessage {
  name: 'ApplicationAPI';
  data: {
    [property: string]: any;
  };
}

/**
 * The response to an API call to the application.
 */
export interface AppAPIResponse extends AppMessageResponse {
  name: 'ApplicationAPI';
  data?: any;
  success: boolean;
  warnings?: string[];
  errors?: string[];
}

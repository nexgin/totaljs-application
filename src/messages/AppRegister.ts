import { ModuleState } from '../module/shared';
import { AppMessage, AppMessageResponse } from './AppMessage';

export interface AppRegisterMessage extends AppMessage {
  name: 'ApplicationModuleRegister';
  data: {
    state: ModuleState;
    label: string;
    name: string;
  };
}
export interface AppRegisterResponse extends AppMessageResponse {
  name: 'ApplicationModuleRegister';
}

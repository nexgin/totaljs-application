import { AppMessage, AppMessageResponse } from './AppMessage';

export interface ModAPIMessage extends AppMessage {
  name: 'ModuleAPI';
  data: {
    [property: string]: any;
  };
}
export interface ModAPIResponse extends AppMessageResponse {
  name: 'ModuleAPI';
  data: {
    data?: any;
    success: boolean;
    warnings?: string[];
    errors?: string[];
  };
}

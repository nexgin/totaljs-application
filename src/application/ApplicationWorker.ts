import { Job, Worker } from 'bullmq';
import * as _ from 'lodash';
import { FlowMessage } from 'total4';
import { Application } from '../Application';
import {
  AppHeartbeatMessage, AppInputResponse,
  AppMessages,
  AppModMessage,
  AppModMessageResponse, AppModQuery, AppModQueryResponse,
  AppRegisterMessage,
  AppUnregisterMessage,
  ModAPIMessage,
  ModAPIResponse
} from '../messages';

/**
 * ApplicationWorker Class
 * This class is responsible for handling tasks sent to the application via BullMQ queues.
 * @class
 * @classdesc ApplicationWorker Class
 * @param app - Application
 * @returns ApplicationWorker
 */
export class ApplicationWorker {
  /**
   * @property worker - BullMQ Worker
   */
  worker?: Worker;

  /**
   * @property instanceWorker - BullMQ Instance Worker
   */
  instanceWorker?: Worker;

  /**
   * Constructor for ApplicationWorker Class
   * - Binds methods used by the class
   * @param app - Application
   * @returns ApplicationWorker
   */
  constructor(public app: Application) {
    this.workHandler = this.workHandler.bind(this);
    this.drained = this.drained.bind(this);
    this.completed = this.completed.bind(this);
    this.failed = this.failed.bind(this);
  }

  /**
   * Initialize the Application Worker
   * Called during Application startup
   * - Stops the Application Worker to ensure no conflicts
   * - Creates the BullMQ Worker
   * - Creates the BullMQ Instance Worker
   * - Binds event handlers for the Worker
   * - Binds event handlers for the Instance Worker
   */
  async init() {
    await this.stop();
    this.worker = new Worker(
      this.app.config.name,
      this.workHandler,
      { connection: this.app.redisClient, concurrency: this.app.config.concurrency || 10 }
    );
    this.instanceWorker = new Worker(
      `${this.app.config.name}_${this.app.instance.id}`,
      this.workHandler,
      { connection: this.app.redisClient, concurrency: this.app.config.concurrency || 10 }
    );
    this.worker.on('drained', this.drained);
    this.worker.on('completed', this.completed);
    this.worker.on('failed', this.failed);
    this.instanceWorker.on('drained', this.drained);
    this.instanceWorker.on('completed', this.completed);
    this.instanceWorker.on('failed', this.failed);
    return true;
  }

  /**
   * Boot the Application Worker
   * Called during Application startup
   * Currently empty but may be used in the future
   */
  async boot() {
    // TODO document why this async method 'boot' is empty
    return true;
  }

  /**
   * Start the Application Worker
   * Called during Application startup
   * Currently empty but may be used in the future
   */
  async start() {
    // TODO document why this async method 'start' is empty
    return true;
  }

  /**
   * Ready the Application Worker
   * Called during Application startup
   * Currently empty but may be used in the future
   */
  async ready() {
    // TODO document why this async method 'ready' is empty
    return true;
  }

  /**
   * Stop the Application Worker
   * Called during Application startup
   * - Stops the BullMQ Worker
   * - Stops the BullMQ Instance Worker
   */
  async stop() {
    if (this.worker) {
      this.worker.off('drained', this.drained);
      this.worker.off('completed', this.completed);
      this.worker.off('failed', this.failed);
      await this.worker.close();
      this.worker = undefined;
    }
    if (this.instanceWorker) {
      this.instanceWorker.off('drained', this.drained);
      this.instanceWorker.off('completed', this.completed);
      this.instanceWorker.off('failed', this.failed);
      await this.instanceWorker.close();
      this.instanceWorker = undefined;
    }
    return true;
  }

  /**
   * Handle tasks from the BullMQ queue sent to the application
   * - Increments the pending task count
   * - Converts the BullMQ Job to an AppMessage
   * - If the message is a register message, calls the registerHandler
   * - If the message is an unregister message, calls the unregisterHandler
   * - If the message is a heartbeat message, calls the heartbeatHandler
   * - If the message is a module message, calls the moduleMessageHandler
   * - If the message is a module query, calls the moduleQueryHandler
   * - If the message is a module input, calls the moduleInputHandler
   * - All other messages are assumed to be module API messages and are sent to the "api" output
   *   with the response being sent back to the sender
   * @param job - Job
   */
  async workHandler(job: Job) {
    if (job.name !== 'ApplicationModuleHeartbeat') {
      console.log(`Got Job ${job.name}`);
    }
    this.app.stats.pending++;
    const appMessage: AppMessages | AppModMessage = {
      name: (job.name as any),
      data: job.data,
      id: (job.id as string),
    };
    if (appMessage.name === 'ApplicationModuleRegister') {
      return await this.registerHandler(appMessage);
    }
    if (appMessage.name === 'ApplicationModuleUnregister') {
      return await this.unregisterHandler(appMessage);
    }
    if (appMessage.name === 'ApplicationModuleHeartbeat') {
      return await this.heartbeatHandler(appMessage);
    }
    if (appMessage.name === 'ApplicationModuleMessage') {
      return await this.moduleMessageHandler(appMessage);
    }
    if (appMessage.name === 'ApplicationModuleQuery') {
      return await this.moduleQueryHandler(appMessage);
    }
    if (appMessage.name === 'ApplicationModuleInput') {
      const input = appMessage.data.data;
      const msg = {
        data: input,
        repo: appMessage.data.meta,
      };
      const phases = await this.app.doNewMsg('phases', msg);
      const processed = await this.app.moduleManager.processInput(phases, msg as FlowMessage);
      const response: AppInputResponse = {
        id: appMessage.id,
        name: appMessage.name,
        success: true,
        data: msg.data,
        meta: msg.repo,
      };
      return response;
    }
    return new Promise((resolve) => {
      const msg: FlowMessage = (this.app.instance as any).newmessage();
      msg.on('end', (m: FlowMessage) => {
        resolve(m.data);
      });
      msg.send('api', appMessage);
    });
  }

  /**
   * Handle the heartbeat message from a module
   * - Calls the moduleManager to do the heartbeat
   * @param appMsg - AppHeartbeatMessage
   * @returns AppHeartbeatResponse
   */
  async heartbeatHandler(appMsg: AppHeartbeatMessage) {
    return await this.app.moduleManager.doHeartbeat(appMsg);
  }

  /**
   * Handle register message from a module
   * - Calls the moduleManager to register
   * @param appMsg - AppRegisterMessage
   * @returns  AppRegisterResponse
   */
  async registerHandler(appMsg: AppRegisterMessage) {
    return await this.app.moduleManager.register(appMsg);
  }

  /**
   * Handle unregister message from a module
   * - Calls the moduleManager to unregister
   * @param appMsg - AppUnregisterMessage
   * @returns AppUnregisterResponse
   */
  async unregisterHandler(appMsg: AppUnregisterMessage) {
    return await this.app.moduleManager.unregister(appMsg);
  }

  /**
   * Handle the module message from a module
   * - Convert AppModMessage to ModAPIMessage
   * - Call the moduleManager to handle the message
   * - Convert ModAPIResponse to AppModMessageResponse
   * - Return the response
   * @param appMsg - AppModMessage
   * @returns AppModMessageResponse
   */
  async moduleMessageHandler(appMsg: AppModMessage) {
    const modMsg: ModAPIMessage = {
      id: GUID(),
      name: 'ModuleAPI',
      data: appMsg.data.data,
    };
    const modResponse: ModAPIResponse = await this.app.moduleManager.call(appMsg.data.name, modMsg);
    const response: AppModMessageResponse = {
      id: appMsg.id,
      name: 'ApplicationModuleMessage',
      success: modResponse.success,
      error: modResponse.error,
      data: modResponse.data,
    };
    return response;
  }

  /**
   * Handle the module query from a module
   * - Create a response
   * - If the query has data, filter the modules by the data
   * - If the query has no data, return all modules
   * - Return the response
   * @param appMsg
   */
  async moduleQueryHandler(appMsg: AppModQuery) {
    const response: AppModQueryResponse = {
      id: appMsg.id,
      name: appMsg.name,
      success: true,
      data: [],
    };
    const modules = this.app.moduleManager.modules.map((m) => { return m.record; });
    if (appMsg.data && _.keys(appMsg.data).length > 0 && !_.isEmpty(appMsg.data)) {
      response.data = _.filter(modules, appMsg.data);
    } else {
      response.data = modules;
    }
    return response;
  }

  /**
   * Handle the drained event from the BullMQ queue
   * - Sends a drained message to the application
   */
  drained() {
    // Queue is drained, no more jobs left
    const msg: FlowMessage = (this.app.instance as any).newmessage();
    msg.send('drained', {
      msg: 'Drained',
    });
  }

  /**
   * Handle the completed event from the BullMQ queue
   * - Decrements the pending task count
   * - Increments the completed task count
   * - Sends a completed message to the application
   * @param job - Job
   */
  completed(job: Job) {
    // job has completed
    this.app.stats.pending--;
    this.app.stats.completed++;
    const msg: FlowMessage = (this.app.instance as any).newmessage();
    msg.send('completed', {
      msg: 'Completed',
      job: job.returnvalue,
    });
  }

  /**
   * Handle the failed event from the BullMQ queue
   * - Decrements the pending task count
   * - Increments the failed task count
   * - Sends a failed message to the application
   * @param _job - Job
   * @param err Error
   */
  failed(_job: Job | undefined, err: Error) {
    // job has failed
    this.app.stats.pending--;
    this.app.stats.failed++;
    const msg: FlowMessage = (this.app.instance as any).newmessage();
    msg.send('failed', {
      msg: 'Failed',
      error: err,
    });
  }

}

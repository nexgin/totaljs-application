import { Queue, QueueEvents } from 'bullmq';
import { EventEmitter } from 'events';
import { ModMessages } from '../messages';
import { ModuleManager } from './ModuleManager';
import { ModuleInstanceRecord, ModuleInstanceStats } from './shared';

/**
 * Module Instance class
 */
export class ModuleInstance extends EventEmitter {
  /**
   * Module instance stats
   */
  stats: ModuleInstanceStats;

  /**
   * BullMQ Queue
   */
  queue?: Queue;

  /**
   * BullMQ QueueEvents
   */
  queueEvents?: QueueEvents;

  /**
   * Module instance constructor
   * - Sets up the module instance stats
   * - Binds the completed and failed event handlers
   * @param manager - Module manager
   * @param record - Module instance record
   */
  constructor(public manager: ModuleManager, public record: ModuleInstanceRecord) {
    super();
    this.stats = {
      id: record.id,
      name: record.name,
      active: record.enabled,
      enabled: record.enabled,
      lastSeen: record.lastSeen,
      pending: 0,
      completed: 0,
      failed: 0,
    };
  }

  /**
   * Initializes the module instance
   * - Sets up the BullMQ Queue and QueueEvents
   * - Sets up the completed and failed event handlers
   */
  async init() {
    this.queue = new Queue(`${this.record.name}_${this.record.label}`, {
      connection: this.manager.app.redisClient,
    });
    this.queueEvents = new QueueEvents(`${this.record.name}_${this.record.label}`, {
      connection: this.manager.app.redisClient,
    });
  }

  /**
   * Stops the module instance
   * - if the queue/queueEvents exist, removes all listeners and closes them
   */
  async stop() {
    if (this.queue) {
      this.queue.removeAllListeners();
      await this.queue.close();
      this.queue = undefined;
    }
    if (this.queueEvents) {
      this.queueEvents.removeAllListeners();
      await this.queueEvents.close();
      this.queueEvents = undefined;
    }
  }

  async update(newRecord: ModuleInstanceRecord) {
    this.record = newRecord;
  }

  /**
   * Calls a module instance
   * - Increments the pending stats
   * - Adds the job to the queue
   * - Waits for the job to finish
   * - Returns the job return value or undefined
   * @param modMessage - Module message
   * @returns Response
   */
  async call(modMessage: ModMessages): Promise<any | undefined> {
    if (this.queue && this.queueEvents) {
      try {
        this.stats.pending++;
        const newJob = await this.queue.add(
          modMessage.name,
          { id: modMessage.id, data: modMessage.data }
        );
        console.log(`Module(${this.record.name}:${this.record.label}) Job Added:`, newJob.id);
        const response = await newJob.waitUntilFinished(this.queueEvents);
        this.stats.pending--;
        this.stats.completed++;
        return response;
      } catch (err) {
        console.error(
          `Module(${this.record.name}) Instance(${this.record.label}) Call Error:`,
          err
        );
        this.stats.pending--;
        this.stats.failed++;
        return undefined;
      }
    }
    return undefined;
  }

}

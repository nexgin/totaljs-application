import * as _ from 'lodash';
import * as RDB from 'rethinkdb';
import { FlowMessage } from 'total4';
import { Application } from '../Application';
import {
  AppHeartbeatMessage,
  AppHeartbeatResponse,
  AppRegisterMessage,
  AppRegisterResponse,
  AppUnregisterMessage,
  AppUnregisterResponse,
  ModInvolvementMessage,
  ModInvolvementResponse,
  ModMessages,
  ModPhaseMessage,
  ModPhaseResponse
} from '../messages';
import { Module } from './Module';
import {
  ModuleInvolvement,
  ModuleProblems,
  ModuleStats,
  PhaseInvolvements,
  PhaseResponses
} from './shared';

/**
 * ModuleManager
 * This class manages the modules of the application
 * @class
 * @param app - The application
 * @returns The new ModuleManager
 */
export class ModuleManager {
  moduleTableName: string = 'Module';
  moduleTableIndexes: string[] = ['name'];
  instanceTableName: string = 'ModuleInstance';
  instanceTableIndexes: string[] = ['name', 'label', 'module'];
  moduleChanges?: RDB.Cursor;

  /**
   * Modules currently loaded
   */
  modules: Module[] = [];

  /**
   * Module Manager Constructor
   * @param app - The application
   * @returns The new Module Manager
   */
  constructor(public app: Application) {
    this.moduleChangeHandler = this.moduleChangeHandler.bind(this);
    this.register = this.register.bind(this);
    this.unregister = this.unregister.bind(this);
    this.doHeartbeat = this.doHeartbeat.bind(this);
  }

  /**
   * Initialize the Module Manager
   * - If the application is connected to RethinkDB, initialize db and tables
   *   - Set up the module and module instance tables
   *   - Set up the module and module instance indexes
   *   - Set up the module change stream
   * @returns True if the Module Manager was initialized, false otherwise
   */
  async init() {
    if (this.app.rethinkdbConnection && this.app.rethinkdbConnected === 'connected') {
      const dbs = await RDB.dbList().run(this.app.rethinkdbConnection);
      if (!_.includes(dbs, this.app.config.name)) {
        await RDB.dbCreate(this.app.config.name).run(this.app.rethinkdbConnection);
        console.log(`Database(${this.app.config.name}) created`);
      }
      const tables = await RDB.db(this.app.config.name).tableList()
        .run(this.app.rethinkdbConnection);
      if (!_.includes(tables, this.moduleTableName)) {
        await RDB.db(this.app.config.name).tableCreate(this.moduleTableName)
          .run(this.app.rethinkdbConnection);
        console.log(`Table(${this.app.config.name}.${this.moduleTableName}) created`);
      }
      if (!_.includes(tables, this.instanceTableName)) {
        await RDB.db(this.app.config.name).tableCreate(this.instanceTableName)
          .run(this.app.rethinkdbConnection);
        console.log(`Table(${this.app.config.name}.${this.instanceTableName}) created`);
      }
      const moduleIndexes = await RDB.db(this.app.config.name).table(this.moduleTableName)
        .indexList().run(this.app.rethinkdbConnection);
      for (const index of this.moduleTableIndexes) {
        if (!_.includes(moduleIndexes, index)) {
          await RDB.db(this.app.config.name).table(this.moduleTableName).indexCreate(index)
            .run(this.app.rethinkdbConnection);
          console.log(`Index(${this.app.config.name}.${this.moduleTableName}.${index}) created`);
        }
      }
      const moduleInstanceIndexes = await RDB.db(this.app.config.name).table(this.instanceTableName)
        .indexList().run(this.app.rethinkdbConnection);
      for (const index of this.instanceTableIndexes) {
        if (!_.includes(moduleInstanceIndexes, index)) {
          await RDB.db(this.app.config.name).table(this.instanceTableName).indexCreate(index)
            .run(this.app.rethinkdbConnection);
          console.log(`Index(${this.app.config.name}.${this.instanceTableName}.${index}) created`);
        }
      }
      const moduleChangeOptions: RDB.ChangesOptions = {
        changefeedQueueSize: 100000,
        includeOffsets: false,
        includeStates: false,
        includeTypes: false,
        squash: false,
        includeInitial: true,
      };
      this.moduleChanges = await RDB.db(this.app.config.name).table(this.moduleTableName)
        .changes(moduleChangeOptions).run(this.app.rethinkdbConnection);
      this.moduleChanges.each(this.moduleChangeHandler);
      return true;
    }
    return false;
  }

  /**
   * Boot the Module Manager
   * Reserved for future use
   */
  async boot() {
    return true;
  }

  /**
   * Start the Module Manager
   * Reserved for future use
   */
  async start() {
    return true;
  }

  /**
   * Ready the Module Manager
   * Reserved for future use
   */
  async ready() {
    return true;
  }

  /**
   * Stop the Module Manager
   * - Stop all modules
   * - Close the module change stream
   */
  async stop() {
    for (const mod of this.modules) {
      await mod.stop();
    }
    if (this.moduleChanges) {
      await this.moduleChanges.close();
    }
    return true;
  }

  /**
   * Handle a module change from the database
   * This function is passed to the RethinkDB Cursor each function
   * https://rethinkdb.com/api/javascript/each
   * - If the module is updated in the database
   *   - If the module is enabled but now disabled, stop the module and remove it from the list
   *   - If the module is disabled but now enabled, load the module and add it to the list
   *   - Otherwise update the module in the list
   * - If the module has been removed from the database, stop the module and remove it from the list
   * - If the module has been added to the database, load the module and add it to the list
   * @param err - The error
   * @param change - The change
   */
  moduleChangeHandler(err: Error, change: any) {
    if (err) {
      console.error('Module Change Error:', err);
      return;
    }
    if (change.old_val && change.new_val) {
      // console.log('Update Module:', change);
      if (change.old_val.enabled && !change.new_val.enabled) {
        // Disable Module
        const mod = _.find(this.modules, (m) => { return (m.record.id === change.old_val.id); });
        if (mod) {
          mod.stop().then(() => {
            console.log(`Module(${mod.record.name}) stopped`);
          });
        }
        _.remove(this.modules, m => m.record.id === change.old_val.id);
        return;
      }
      if (!change.old_val.enabled && change.new_val.enabled) {
        // Enable Module
        const mod = new Module(this, change.new_val);
        this.modules.push(mod);
        mod.init().then(() => {
          console.log(`Module(${mod.record.name}) initialized`);
        });
        return;
      }
      // Update Module
      _.each(
        this.modules,
        (m) => {
          if (m.record.id === change.old_val.id) {
            m.record = change.new_val;
          }
        }
      );
      return;
    }
    if (change.old_val && !change.new_val) {
      console.log('Delete Module:', change);
      _.each(this.modules, (m) => { if (m.record.id === change.old_val.id) { m.stop(); } });
      _.remove(this.modules, m => m.record.id === change.old_val.id);
      return;
    }
    if (change.new_val && !change.old_val) {
      console.log('Create Module:', change);
      if (change.new_val.enabled) {
        const mod = new Module(this, change.new_val);
        this.modules.push(mod);
        mod.init().then(() => {
          console.log(`Module(${mod.record.name}) initialized`);
        }).catch((initErr) => {
          console.error(initErr);
        });
      }
    }
  }

  /**
   * Return stats for all modules
   * @returns Array of ModuleStats
   */
  stats(): ModuleStats[] {
    return this.modules.map(m => m.stats);
  }

  /**
   * Handle an AppHeartbeatMessage message coming from a module
   * - Try to update the lastSeen date of the module instance
   * - If the module instance is not found, throw an error
   * - If the module is found, update the lastSeen date of the module and sync module
   *  - If the module is disabled remove it from the modules array
   * - Return the response
   * @param appMsg - The message
   * @returns The response
   */
  async doHeartbeat(appMsg: AppHeartbeatMessage) {
    const newLastSeen = new Date();
    const response: AppHeartbeatResponse = {
      id: appMsg.id,
      name: 'ApplicationModuleHeartbeat',
      success: false,
    };
    try {
      const mod = _.find(this.modules, (m) => { return (m.record.name === appMsg.data.name); });
      if (!mod) {
        response.error = `Module(${appMsg.data.name}) not found`;
        return response;
      }
      const modInst = _.find(
        mod.instances,
        (i) => { return (i.record.label === appMsg.data.label); }
      );
      if (!modInst) {
        response.error = `ModuleInstance(${appMsg.data.label}) not found`;
        return response;
      }
      await RDB.db(this.app.config.name).table(this.instanceTableName)
        .filter({ label: appMsg.data.label }).update({ lastSeen: newLastSeen, enabled: true })
        .run(this.app.rethinkdbConnection as RDB.Connection);
      await RDB.db(this.app.config.name).table(this.moduleTableName)
        .filter({ name: appMsg.data.name }).update({ lastSeen: newLastSeen, enabled: true })
        .run(this.app.rethinkdbConnection as RDB.Connection);
      response.success = true;
    } catch (err) {
      response.error = (err as Error).message;
      console.error('Heartbeat Failure:', err);
      console.debug('Heartbeat Message:', appMsg);
    }
    return response;
  }

  /**
   * Handle an AppRegisterMessage message coming from a module
   * - Try to find the module in the database
   * - If Module exists
   *   - Try to find the module instance in the database
   *   - If Module Instance exists
   *     - Update the lastSeen date of the module instance
   *     - Update the lastSeen date of the module and ensure it is enabled
   *   - If Module Instance does not exist
   *     - Create a new module instance
   *     - Update the lastSeen date of the module and ensure it is enabled
   * - If Module does not exist
   *   - Create a new disabled module
   *   - Create a new module instance
   *   - Enable the module (To allow the module to be started with enabled instances)
   * - Return the response
   * @param appMsg - The message
   * @returns The response
   */
  async register(appMsg: AppRegisterMessage) {
    const newLastSeen = new Date();
    const response: AppRegisterResponse = {
      id: appMsg.id,
      name: 'ApplicationModuleRegister',
      success: false,
    };
    const updates: {lastSeen: Date, enabled?: boolean} = { lastSeen: newLastSeen, enabled: true };
    try {
      if (this.app.rethinkdbConnection) {
        const modQuery = await RDB.db(this.app.config.name).table(this.moduleTableName)
          .filter({ name: appMsg.data.name }).run(this.app.rethinkdbConnection);
        const modRecs = await modQuery.toArray();
        if (modRecs && modRecs.length > 0) {
          // Module exists
          const instanceQuery = await RDB.db(this.app.config.name).table(this.instanceTableName)
            .filter({ module: modRecs[0].id, label: appMsg.data.label })
            .run(this.app.rethinkdbConnection);
          const instanceRecs = await instanceQuery.toArray();
          if (instanceRecs && instanceRecs.length > 0) {
            // Module Instance exists
            await RDB.db(this.app.config.name).table(this.instanceTableName)
              .get(instanceRecs[0].id).update(updates).run(this.app.rethinkdbConnection);
          } else {
            // Module Instance does not exist
            await RDB.db(this.app.config.name).table(this.instanceTableName).insert({
              enabled: true,
              module: modRecs[0].id,
              label: appMsg.data.label,
              name: appMsg.data.name,
              lastSeen: newLastSeen,
            }).run(this.app.rethinkdbConnection);
          }
          await RDB.db(this.app.config.name).table(this.moduleTableName).get(modRecs[0].id)
            .update(updates).run(this.app.rethinkdbConnection);
          response.success = true;
        } else {
          // Module does not exist
          const insertModule = await RDB.db(this.app.config.name).table(this.moduleTableName)
            .insert({
              name: appMsg.data.name,
              lastSeen: newLastSeen,
              enabled: false,
            }).run(this.app.rethinkdbConnection);
          if (insertModule.inserted === 1) {
            const modId = insertModule.generated_keys[0];
            const insertInstance = await RDB.db(this.app.config.name)
              .table(this.instanceTableName)
              .insert({
                enabled: true,
                module: modId,
                label: appMsg.data.label,
                name: appMsg.data.name,
                lastSeen: newLastSeen,
              }).run(this.app.rethinkdbConnection);
            if (insertInstance.inserted === 1) {
              await RDB.db(this.app.config.name).table(this.moduleTableName).get(modId)
                .update({ enabled: true }).run(this.app.rethinkdbConnection);
              response.success = true;
            }
          }
        }
      }
    } catch (e) {
      response.error = e?.toString();
    }
    return response;
  }

  /**
   * Handle an AppUnregisterMessage message coming from a module
   * - Try to find the module in the database
   * - If Module exists
   *   - Try to find the module instance in the database
   *   - If Module Instance exists
   *    - Remove the module instance from the module
   *    - Sync the module with the database
   *    - If the module is disabled remove it from the modules array
   *    - Set response.success to true
   * - If response.success is false, set response.error to 'Not found'
   * - Return the response
   * @param appMsg - The message
   * @returns The response
   */
  async unregister(appMsg: AppUnregisterMessage) {
    const response: AppUnregisterResponse = {
      id: appMsg.id,
      name: 'ApplicationModuleUnregister',
      success: false,
    };
    const mod = this.modules.find(m => m.record.name === appMsg.data.name);
    if (mod) {
      const instance = mod.instances.find(i => i.record.label === appMsg.data.label);
      if (instance) {
        await mod.remove(instance.record.id);
        response.success = true;
      } else {
        response.error = `Module(${appMsg.data.name}) Instance(${appMsg.data.label}) Not found`;
      }
    }
    if (!response.success) {
      response.error = `Module(${appMsg.data.name}) Not found`;
    }
    return response;
  }

  /**
   * Make a call to a module
   * - Find the module by name
   * - If the module is found, proxy the call to the module call
   * - If the module is not found, throw an error
   * @param name - The name of the module
   * @param modMsg - The message to send
   */
  async call(name: string, modMsg: ModMessages) {
    const mod = this.modules.find(m => m.record.name === name);
    if (mod) {
      return mod.call(modMsg);
    }
    throw new Error(`Module ${name} not found`);
  }

  /**
   * Make a call to a module instance
   * - Find the module by name
   * - If the module is found, proxy the call to the module callInstance
   * - If the module is not found, throw an error
   * @param name - The name of the module
   * @param instanceId - The instance id
   * @param modMsg - The message to send
   */
  async callInstance(name: string, instanceId: string, modMsg: ModMessages) {
    const mod = this.modules.find(m => m.record.name === name);
    if (mod) {
      return mod.callInstance(instanceId, modMsg);
    }
    throw new Error(`Module ${name} not found`);
  }

  /**
   * Ask each module for its involvement in the given phases
   * - Create a ModInvolvementMessage with the given phases and message.repo as meta
   * - Create a moduleInvolvements object to store the results
   * - For each module
   *   - Call the module with the ModInvolvementMessage
   *   - If the response is successful
   *     - If the response has phases
   *       - Add the module and its phases to the moduleInvolvements object
   *     - If the response has meta
   *       - Set message.repo to the response meta using _.defaults to merge the two objects
   * - Return the moduleInvolvements object
   * @param phases - The phases to check
   * @param message - The message
   * @returns The modules involvement
   */
  async getModulesInvolvement(phases: string[], message: FlowMessage) {
    const involvementMsg: ModInvolvementMessage = {
      id: GUID(),
      name: 'ModuleInvolvement',
      data: { phases, meta: message.repo },
    };
    const moduleInvolvements: {[module: string]: {[phase: string]: number}} = {};
    for (const mod of this.modules) {
      const involvementResponse: ModInvolvementResponse = await mod.call(involvementMsg);
      if (involvementResponse.success) {
        if (_.keys(involvementResponse.data.phases).length > 0) {
          moduleInvolvements[mod.record.name] = involvementResponse.data.phases;
        }
        if (involvementResponse.data.meta) {
          message.repo = _.defaults(involvementResponse.data.meta, message.repo);
        }
      }
    }
    return moduleInvolvements;
  }

  /**
   * Sort the modules by involvement priorities
   * @param involvements - The involvements
   * @returns The sorted involvements
   */
  sortModulesByInvolvement(involvements: {[phase:string]: {[module: string]: number}}) {
    for (const phase in involvements) {
      if (involvements.hasOwnProperty(phase)) {
        const modules = involvements[phase];
        const sorted = Object.keys(modules).sort((a, b) => modules[b] - modules[a]);
        involvements[phase] = {};
        for (const module of sorted) {
          involvements[phase][module] = modules[module];
        }
      }
    }
    return involvements;
  }

  /**
   * Get the involvement of each module in the given phases then sort them by priority
   * - Call getModulesInvolvement to get the modules involvement
   * - Create an involvement object to store the results
   * - For each module
   *   - For each phase
   *     - Add the module and its involvement to the involvement object
   * - Sort the modules by involvement priorities
   * - Return the involvement object
   * @param phases - The phases to check
   * @param message - The message
   * @returns The involvements
   */
  async getInvolvement(phases: string[], message: FlowMessage): Promise<PhaseInvolvements> {
    const moduleInvolvements = await this.getModulesInvolvement(phases, message);
    let involvement: PhaseInvolvements = {};
    for (const moduleName in moduleInvolvements) {
      if (moduleInvolvements.hasOwnProperty(moduleName)) {
        const moduleInvolvement = moduleInvolvements[moduleName];
        for (const phase in moduleInvolvement) {
          if (moduleInvolvement.hasOwnProperty(phase)) {
            if (!involvement[phase]) {
              involvement[phase] = {};
            }
            involvement[phase][moduleName] = moduleInvolvement[phase];
          }
        }
      }
    }
    involvement = this.sortModulesByInvolvement(involvement);
    return involvement;
  }

  /**
   * Process Application Input through a specific module
   * - Create a ModPhaseMessage with the given phase, message.data as input and message.repo as meta
   * - Call the module with the ModPhaseMessage
   * - If the response is successful
   *   - If the response has output
   *     - Set message.data to the response output
   *   - If the response has meta
   *     - Set message.repo to the response meta using _.defaults to merge the two objects
   * - Return the response
   * @param module - The module
   * @param phase - The phase
   * @param message - The message
   * @returns The response
   */
  async processInputModule(module: Module, phase: string, message: FlowMessage) {
    const modPhaseMessage: ModPhaseMessage = {
      id: GUID(),
      name: 'ModulePhase',
      data: {
        phase,
        input: message.data,
        meta: message.repo,
      },
    };
    const response: ModPhaseResponse = await module.call(modPhaseMessage);
    if (response) {
      if (response.data.output) {
        message.data = response.data.output;
      }
      if (response.data.meta) {
        message.repo = _.defaults(response.data.meta, message.repo);
      }
    }
    return response;
  }

  /**
   * Process Application Input through all modules
   * - Create a warnings and errors object to store the problems
   * - Create a stop boolean to store if the phase should stop
   * - For each module
   *   - Call processInputModule to process the module
   *   - If the response has warnings
   *     - Add the module and its warnings to the warnings object
   *   - If the response has errors
   *     - Add the module and its errors to the errors object
   *   - If the response has stop === true
   *     - Set stop to true
   *     - Break the loop
   * Return object containing the warnings, errors and stop properties
   * @param modules - The modules
   * @param phase - The phase
   * @param message - The message
   * @returns The response
   */
  async processInputModules(modules: ModuleInvolvement, phase: string, message: FlowMessage) {
    const warnings: ModuleProblems = {};
    const errors: ModuleProblems = {};
    let stop = false;
    for (const moduleName in modules) {
      if (modules.hasOwnProperty(moduleName)) {
        const module = this.modules.find(m => m.record.name === moduleName);
        if (module) {
          const response = await this.processInputModule(module, phase, message);
          if (response?.data?.warnings) {
            warnings[moduleName] = response.data.warnings;
          }
          if (response?.data?.errors) {
            errors[moduleName] = response.data.errors;
          }
          if (response?.data?.stop === true) {
            console.log('Stopping Phase:', phase, 'due to stop flag from module:', moduleName);
            stop = true;
            break;
          }
        }
      }
    }
    return { warnings, errors, stop };
  }

  /**
   * Process Application Input
   * - Call syncModules to make sure the modules are up-to-date
   * - Call getInvolvement to get the involvement of each module in the given phases
   * - Create a phaseResponses object to store the responses
   * - For each phase
   *   - Call processInputModules to process the modules
   *   - If the response has stop === true
   *     - Break the loop
   * - Return the phaseResponses object
   * @param phases - The phases to check
   * @param message - The message
   * @returns The responses
   */
  async processInput(phases: string[], message: FlowMessage): Promise<PhaseResponses> {
    const involvement = await this.getInvolvement(phases, message);
    const phaseResponses: PhaseResponses = {};
    for (const phase in involvement) {
      if (involvement.hasOwnProperty(phase)) {
        const modules = involvement[phase];
        console.log('Executing Phase:', phase, 'with modules:', Object.keys(modules));
        phaseResponses[phase] = await this.processInputModules(modules, phase, message);
        if (phaseResponses[phase].stop) {
          console.log('Stopping Phase:', phase, 'due to stop flag');
          break;
        }
      }
    }
    return phaseResponses;
  }
}

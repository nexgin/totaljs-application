/**
 * Application Configuration
 */
export interface ApplicationConfig {
  name: string;
  mode: 'all' | 'input' | 'api';
  concurrency: number;
  // mongo?: string;
  rethinkdb?: string;
  redis?: string;
}

/**
 * Module stats
 */
export interface ModuleStats {
  name: string;
  pending: number;
  completed: number;
  failed: number;
  active: boolean;
  enabled: boolean;
  lastSeen: Date;
}

/**
 * Module instance stats
 */
export interface ModuleInstanceStats {
  id: string;
  name: string;
  pending: number;
  completed: number;
  failed: number;
  active: boolean;
  enabled: boolean;
  lastSeen: Date;
}

/**
 * Application stats
 */
export interface ApplicationStats {
  pending: number;
  completed: number;
  failed: number;
  modules: ModuleStats[];
}

/**
 * Application state
 */
export type ApplicationState = 'unconfigured' |
  'configuring' |
  'configured' |
  'initializing' |
  'initialized' |
  'booting' |
  'booted' |
  'starting' |
  'started' |
  'running' |
  'error' |
  'stopping' |
  'stopped';

/**
 * Module record
 */
export interface ModuleRecord {
  id: string;
  name: string;
  enabled: boolean;
  lastSeen: Date;
}

/**
 * Module instance record
 */
export interface ModuleInstanceRecord {
  id: string;
  module: string;
  label: string;
  name: string;
  enabled: boolean;
  lastSeen: Date;
}

/**
 * Interface representing the data format of the module involvement
 */
export interface ModuleInvolvement {
  [module: string]: number;
}

/**
 * Interface representing the data format of the phase involvement
 */
export interface PhaseInvolvements {
  [phase: string]: ModuleInvolvement;
}

/**
 * Interface representing the data format of module problems
 */
export interface ModuleProblems {
  [module: string]: string[];
}

/**
 * Interface representing the data format of phase responses when processing application input
 */
export interface PhaseResponses {
  [phase: string]: {
    warnings: ModuleProblems,
    errors: ModuleProblems,
    stop: boolean
  };
}

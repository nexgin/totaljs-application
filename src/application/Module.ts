import { Queue, QueueEvents } from 'bullmq';
import { EventEmitter } from 'events';
import * as _ from 'lodash';
import * as RDB from 'rethinkdb';
import { ModMessages } from '../messages';
import { ModuleInstance } from './ModuleInstance';
import { ModuleManager } from './ModuleManager';
import { ModuleInstanceRecord, ModuleRecord, ModuleStats } from './shared';

/**
 * Module class
 * This class represents a module and provides functionality necessary for it to operate
 * @class
 * @extends EventEmitter
 */
export class Module extends EventEmitter {
  /**
   * Module instances
   */
  instances: ModuleInstance[] = [];

  timeoutTimer?: NodeJS.Timeout;
  instanceChanges?: RDB.Cursor;

  /**
   * Module stats
   */
  stats: ModuleStats;

  /**
   * BullMQ Queue
   */
  queue?: Queue;

  /**
   * BullMQ QueueEvents
   */
  queueEvents?: QueueEvents;

  /**
   * Module constructor
   * - Sets up the module stats
   * - Binds the completed and failed event handlers
   * @param manager - Module manager
   * @param record - Module record
   */
  constructor(public manager: ModuleManager, public record: ModuleRecord) {
    super();
    this.stats = {
      name: record.name,
      active: record.enabled,
      enabled: record.enabled,
      lastSeen: record.lastSeen,
      pending: 0,
      completed: 0,
      failed: 0,
    };
    this.instanceChangeHandler = this.instanceChangeHandler.bind(this);
  }

  /**
   * Adds a new module instance
   * - Creates a new module instance
   * - Adds the instance to the list of instances
   * - Initializes the instance
   * - Returns the instance
   * @param record - Module instance record
   */
  async add(record: ModuleInstanceRecord) {
    const instance = new ModuleInstance(this.manager, record);
    this.instances.push(instance);
    await instance.init();
    return instance;
  }

  /**
   * Removes a module instance
   * - Removes the instance from the database
   * - Looks up the instance in the list of instances
   * - If found, stop and remove the instance
   * - If no more instances, disable module and stop it
   * - Returns the removed instance or undefined if not found
   * @param id - Module instance id
   */
  async remove(id: string) {
    const removed = _.remove(this.instances, instance => instance.record.id === id);
    if (removed.length > 0) {
      await removed[0].stop();
    }
    if (this.count() === 0) {
      await RDB.db(this.manager.app.config.name).table(this.manager.moduleTableName)
        .get(this.record.id).update({ enabled: false })
        .run(this.manager.app.rethinkdbConnection as RDB.Connection);
    }
    return removed[0];
  }

  /**
   * Updates a module instance
   * - Looks up the instance in the list of instances
   * - If found, update the instance record
   * @param id - Module instance id
   * @param record - Module instance record
   */
  async update(id: string, record: ModuleInstanceRecord) {
    const index = this.instances.findIndex(
      instance => instance.record.id === id
    );
    if (index >= 0) {
      await this.instances[index].update(record);
      this.stats.lastSeen = record.lastSeen;
      return this.instances[index];
    }
    return undefined;
  }

  /**
   * Counts the number of module instances
   * @returns Number of module instances
   */
  count() {
    return this.instances.length;
  }

  instanceChangeHandler(err: Error, change: any) {
    if (err) {
      console.error(`Module(${this.record.name}) Instance Change Error:`, err);
      return;
    }
    if (!change.old_val && change.new_val) {
      // Add
      this.add(change.new_val).finally(() => {
        console.debug(
          `Module(${this.record.name}) Instance(${change.new_val.id}) Added:`,
          change.new_val
        );
      });
    }
    if (change.old_val && change.new_val) {
      // Update
      this.update(change.new_val.id, change.new_val).finally(() => {
        // console.debug('Module Instance Updated:', change.new_val);
      });
    }
    if (change.old_val && !change.new_val) {
      // Remove
      this.remove(change.old_val.id).finally(() => {
        console.debug(
          `Module(${this.record.name}) Instance(${change.old_val.id}) Removed:`,
          change.old_val
        );
      });
    }
  }

  /**
   * Initializes the module
   * - Syncs the module instances with the database
   * - If module is enabled
   *   - Create a new BullMQ Queue
   *   - Create a new BullMQ QueueEvents
   *   - Bind the completed and failed event handlers
   */
  async init() {
    if (this.record.enabled && this.manager.app.rethinkdbConnection) {
      this.timeoutTimer = setTimeout(async () => {
        if (this.manager.app.rethinkdbConnection) {
          const expired = _.filter(this.instances, (instance) => {
            return instance.record.lastSeen.getTime() < Date.now() - 60000;
          });
          if (expired.length > 0) {
            console.warn(
              `Module(${this.record.name}) Instance(s) Expired:`,
              expired.map(instance => instance.record.id)
            );
            for (const instance of expired) {
              await RDB.db(this.manager.app.config.name).table(this.manager.instanceTableName)
                .get(instance.record.id).delete().run(this.manager.app.rethinkdbConnection);
            }
          }
        }
      }, 1000);
      const instanceChangeOptions: RDB.ChangesOptions = {
        changefeedQueueSize: 100000,
        includeOffsets: false,
        includeStates: false,
        includeTypes: false,
        squash: false,
        includeInitial: true,
      };
      this.instanceChanges = await RDB.db(this.manager.app.config.name)
        .table(this.manager.instanceTableName).filter({ module: this.record.id })
        .changes(instanceChangeOptions).run(this.manager.app.rethinkdbConnection);
      this.instanceChanges.each(this.instanceChangeHandler);
      this.queue = new Queue(this.record.name, {
        connection: this.manager.app.redisClient,
      });
      this.queueEvents = new QueueEvents(this.record.name, {
        connection: this.manager.app.redisClient,
      });
    }
  }

  /**
   * Stops the module
   * - If queue exists, close it
   * - If queueEvents exists, close it
   */
  async stop() {
    if (this.queue) {
      await this.queue.close();
    }
    if (this.queueEvents) {
      await this.queueEvents.close();
    }
    if (this.timeoutTimer) {
      clearTimeout(this.timeoutTimer);
    }
  }

  /**
   * Sends a message to the module
   * - If queue and queueEvents exist
   *   - Increment pending stats
   *   - Add job to queue with the name as modMessage.name and data as modMessage.data
   *   - Wait for job to finish
   * - Returns the job result or undefined if queue or queueEvents do not exist
   * @param modMessage - Module message
   * @returns Module response
   */
  async call(modMessage: ModMessages): Promise<any | undefined> {
    if (this.queue && this.queueEvents) {
      try {
        this.stats.pending++;
        const newJob = await this.queue.add(modMessage.name, modMessage.data);
        console.log(`Module(${this.record.name}) Job Added:`, newJob.id);
        const response = await newJob.waitUntilFinished(this.queueEvents);
        this.stats.pending--;
        this.stats.completed++;
        return response;
      } catch (err) {
        console.error('Module Call Error:', err);
        this.stats.pending--;
        this.stats.failed++;
        return undefined;
      }
    }
    return undefined;
  }

  /**
   * Sends a message to a specific module instance
   * - Looks up the module instance in the list of instances
   * - If found, proxy the call to the module instance
   * - Returns the module instance response or undefined if not found
   * @param instanceId - Module instance id
   * @param appMsg - Module message
   * @returns Module response
   */
  async callInstance(instanceId: string, appMsg: ModMessages) {
    const instance = this.instances.find(inst => inst.record.id === instanceId);
    if (instance) {
      return await instance.call(appMsg);
    }
    return undefined;
  }

}
